# Define Droplets (VMs) to create and the project to put them in.
resource "digitalocean_droplet" "web" {
  image  = "ubuntu-20-04-x64"
  name   = "sifted-nodejs-api"
  region = "nyc1"
  size   = "s-1vcpu-1gb"
  ssh_keys = [
    data.digitalocean_ssh_key.ssh_key.id
  ]
}

resource "digitalocean_project_resources" "terraform_rs" {
  project = data.digitalocean_project.dotf.id
  resources = [
     digitalocean_droplet.web.urn
  ]
}


# {{{   Regions:
# ---------
#  NYC1 	New York City, United States 	nyc1
#  NYC2 	New York City, United States 	nyc2*
#  NYC3 	New York City, United States 	nyc3
#  AMS2 	Amsterdam, the Netherlands  	ams2
#  AMS3 	Amsterdam, the Netherlands  	ams3
#  SFO1 	San Francisco, United States 	sfo1*
#  SFO2 	San Francisco, United States 	sfo2*
#  SFO3 	San Francisco, United States 	sfo3
#  SGP1 	Singapore 	                    sgp1
#  LON1 	London, United Kingdom 	        lon1
#  FRA1 	Frankfurt, Germany 	            fra1
#  TOR1 	Toronto, Canada 	            tor1
#  BLR1 	Bangalore, India 	            blr1
# * Not currently available.
# }}}


# {{{   Operating System:
# ------------------
# ID          Name                  Type        Distribution    Slug                   Public    Min Disk
# 93524084    18.04 (LTS) x64       snapshot    Ubuntu          ubuntu-18-04-x64       true      15
# 93525508    20.04 (LTS) x64       snapshot    Ubuntu          ubuntu-20-04-x64       true      15
# 92517214    11 x64                snapshot    Debian          debian-11-x64          true      15
# 86718194    10 x64                snapshot    Debian          debian-10-x64          true      15
# 77558491    12.2 ufs x64          snapshot    FreeBSD         freebsd-12-x64-ufs     true      20
# 77558552    12.2 zfs x64          snapshot    FreeBSD         freebsd-12-x64-zfs     true      15
# 89199942    RockyLinux 8.4 x64    snapshot    Rocky Linux     rockylinux-8-x64       true      15
# 85779954    8 x64                 snapshot    CentOS          centos-8-x64           true      15
# 85722003    7 x64                 snapshot    CentOS          centos-7-x64           true      15
# 89246461    8 Stream x64          snapshot    CentOS          centos-stream-8-x64    true      15
# 95344509    35 x64                snapshot    Fedora          fedora-35-x64          true      15
# 84780898    34 x64                snapshot    Fedora          fedora-34-x64          true      15
# 78547182    1.5.8 x64             snapshot    RancherOS       rancheros              true      15
# }}}


# {{{  Size
# ---------
# Slug                  Memory    VCPUs    Disk    Price Monthly    Price Hourly
# s-1vcpu-1gb           1024      1        25      5.00             0.007440
# s-1vcpu-2gb           2048      1        50      10.00            0.014880
# s-2vcpu-2gb           2048      2        60      15.00            0.022320
# s-2vcpu-4gb           4096      2        80      20.00            0.029760
# s-4vcpu-8gb           8192      4        160     40.00            0.059520
# s-1vcpu-1gb-{amd,intel}       1024      1        25      6.00             0.008930
# s-1vcpu-2gb-{amd,intel}       2048      1        50      12.00            0.017860
# s-2vcpu-2gb-{amd,intel}       2048      2        60      18.00            0.026790
# s-2vcpu-4gb-{amd,intel}       4096      2        80      24.00            0.035710
# c-2                   4096      2        25      40.00            0.059520
# c2-2vcpu-4gb          4096      2        50      45.00            0.066960
# g-2vcpu-8gb           8192      2        25      60.00            0.089290
# gd-2vcpu-8gb          8192      2        50      65.00            0.096730
# s-8vcpu-16gb          16384     8        320     80.00            0.119050
# m-2vcpu-16gb          16384     2        50      80.00            0.119050
# c-4                   8192      4        50      80.00            0.119050
# c2-4vcpu-8gb          8192      4        100     90.00            0.133930
# m3-2vcpu-16gb         16384     2        150     100.00           0.148810
# g-4vcpu-16gb          16384     4        50      120.00           0.178570
# so-2vcpu-16gb         16384     2        300     125.00           0.186010
# m6-2vcpu-16gb         16384     2        300     125.00           0.186010
# gd-4vcpu-16gb         16384     4        100     130.00           0.193450
# so1_5-2vcpu-16gb      16384     2        450     155.00           0.230650
# m-4vcpu-32gb          32768     4        100     160.00           0.238100
# c-8                   16384     8        100     160.00           0.238100
# c2-8vcpu-16gb         16384     8        200     180.00           0.267860
# m3-4vcpu-32gb         32768     4        300     195.00           0.290180
# g-8vcpu-32gb          32768     8        100     240.00           0.357140
# so-4vcpu-32gb         32768     4        600     250.00           0.372020
# m6-4vcpu-32gb         32768     4        600     250.00           0.372020
# gd-8vcpu-32gb         32768     8        200     260.00           0.386900
# }}}

# vim: foldmethod=marker
