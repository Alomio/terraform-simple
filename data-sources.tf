# file: ${proj_base}/data-sources.tf
# Data allows you to query Digital Ocean for objects or run weird shit like python

data "external" "droplet_name" {
  program = ["python3", "${path.module}/external/name-generator.py"]
}

data "digitalocean_project" "dotf" {
  name        = "terraform"
}

data "digitalocean_ssh_key" "ssh_key" {
  name = "dmc_laptop"
}

