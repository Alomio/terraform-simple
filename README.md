# Terraform Project

This is an example Terraform project to spin up a simple VM in Digital Ocean.

## Requirements

 - Install Terrafom
 - Acquire an API token from DigitalOcean

## Run 
You can create a VM after you have created a file named `dotoken` with your digital ocean token inside. Then:

```
$ terraform init
$ ./tapply.sh
```

## Documentation 
This was largly taken form:
 - https://www.digitalocean.com/community/tutorials/how-to-structure-a-terraform-project

